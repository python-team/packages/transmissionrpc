python-transmission-rpc (7.0.11-3) unstable; urgency=medium

  * Team upload.
  * debian/control: Drop my name from the uploaders list.

 -- Boyuan Yang <byang@debian.org>  Sun, 19 Jan 2025 19:32:41 -0500

python-transmission-rpc (7.0.11-2) unstable; urgency=medium

  * Source-only upload to allow testing migration.

 -- Boyuan Yang <byang@debian.org>  Wed, 25 Sep 2024 15:19:00 -0400

python-transmission-rpc (7.0.11-1) unstable; urgency=medium

  * Rename source package from transmissionrpc to python-transmission-rpc
    following upstream author's request. (Closes: #1082277)
  * debian/control: Add myself into the uploaders list.
  * debian/control: Rename binary packages accordingly.
  * debian/*.dirs: Dropped, not needed.
  * debian/copyright: Update copyright information.

 -- Boyuan Yang <byang@debian.org>  Fri, 20 Sep 2024 16:02:01 -0400

transmissionrpc (7.0.11-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Sun, 08 Sep 2024 13:46:21 +0100

transmissionrpc (7.0.5-1) unstable; urgency=medium

  * Team upload.

  [ Alexandre Detiste ]
  * New upstream version 7.0.5 (closes: #1071724)

  [ Colin Watson ]
  * Skip new test_groups test, which requires a running Transmission
    service.
  * Update Homepage.

 -- Colin Watson <cjwatson@debian.org>  Tue, 03 Sep 2024 21:57:13 +0100

transmissionrpc (7.0.3-2) unstable; urgency=medium

  * Team upload.
  * fix autopkgtest autodep8 integration

 -- Alexandre Detiste <tchet@debian.org>  Tue, 13 Feb 2024 22:54:14 +0100

transmissionrpc (7.0.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 7.0.3 (Closes: #1058438)
  * transmissionrpc is not maintained since 2013,
    switch to live fork transmission-rpc (Closes: #979626)
  * switch to debhelper-compat 13
  * remove dependency on python3-six
  * add build dependencies:
    * pybuild-plugin-pyproject
    * python3-poetry-core
    * python3-yarl
  * set Rules-Requires-Root: no
  * set Standards-Version: 4.6.2

  [ Alexandre Rossi ]
  * port helical to new upstream API

 -- Alexandre Detiste <tchet@debian.org>  Sun, 11 Feb 2024 17:02:49 +0100

transmissionrpc (0.11-7) unstable; urgency=medium

  * Team upload.
  * Run tests with pytest instead of setup.py test.

 -- Athos Ribeiro <athoscribeiro@gmail.com>  Sun, 18 Dec 2022 12:38:20 -0300

transmissionrpc (0.11-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + python3-transmissionrpc: Drop versioned constraint on transmission-daemon
      in Suggests.
    + python-transmissionrpc-doc: Drop versioned constraint on
      python-transmissionrpc in Replaces.
    + python-transmissionrpc-doc: Drop versioned constraint on
      python-transmissionrpc in Breaks.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 04:01:55 +0100

transmissionrpc (0.11-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-transmissionrpc-doc: Add Multi-Arch: foreign.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 21:45:28 -0400

transmissionrpc (0.11-4) unstable; urgency=medium

  * Team upload.

  [ Vincent Bernat ]
  * d/watch: update URL.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Rename d/tests/control.autodep8 to d/tests/control.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support (Closes: #681074).
  * Fix helical.py to support Python 3.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * Remove Timur Birsh from Uploders (Closes: #879874).
  * d/rules: Remove SPHINXOPTS (no needed anymore).

 -- Ondřej Nový <onovy@debian.org>  Sat, 03 Aug 2019 13:11:10 +0200

transmissionrpc (0.11-3) unstable; urgency=medium

  * Team upload.
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/rules: Set PYBUILD_NAME
  * Bump debhelper compat version to 10
  * Remove d/patches/01_setup.patch (Closes: #851247)
  * Add python3-setuptools to B-D
  * Add autopkgtest-pkg-python testsuite
  * Add CLI autopkgtest
  * Standards-Version is 3.9.8 now (no changes needed)
  * Remove python{,3}-nose from B-D and use pybuild auto_test
  * Add python{3,}-all to B-D, to run tests on all Python versions

 -- Ondřej Nový <onovy@debian.org>  Fri, 13 Jan 2017 13:32:54 +0100

transmissionrpc (0.11-2) unstable; urgency=medium

  * Make the build reproducible. Patch from Juan Picca.
    Closes: #788598.
  * Bump Standards-Version to 3.9.6.

 -- Vincent Bernat <bernat@debian.org>  Thu, 20 Aug 2015 10:27:48 +0200

transmissionrpc (0.11-1) unstable; urgency=low

  * New upstream release.
  * Documentation has moved to python-transmissionrpc-doc. Add the
    appropriate dependencies for that. Closes: #720045.

 -- Vincent Bernat <bernat@debian.org>  Fri, 01 Nov 2013 12:21:02 +0100

transmissionrpc (0.10-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Vincent Bernat ]
  * New upstream release.
  * Bump Standards-Version to 3.9.4.
  * Switch to dh-python.
  * Add Python 3 package.

 -- Vincent Bernat <bernat@debian.org>  Fri, 16 Aug 2013 21:21:19 +0200

transmissionrpc (0.8-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.3.
  * Switch to debhelper minimal ruleset.
  * Compile documentation with Sphinx.

 -- Vincent Bernat <bernat@debian.org>  Sat, 23 Jun 2012 17:43:37 +0200

transmissionrpc (0.7-1) unstable; urgency=low

  [ Vincent Bernat ]
  * New upstream release. Support of transmission up to version 2.12.
  * Watch for upstream tar.gz instead of zip.

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Wed, 09 Mar 2011 08:31:16 +0100

transmissionrpc (0.5-1) unstable; urgency=low

  [ Vincent Bernat ]
  * New upstream release. Compatibility with Transmission 1.9 and newer.
  * Bump Standards-Version to 3.9.1.
  * Switch to 3.0 (quilt) format.
  * Remove some of the contrib scripts which are not shipped anymore.

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Mon, 30 Aug 2010 22:37:04 +0200

transmissionrpc (0.4-1) unstable; urgency=low

  [ Luca Falavigna ]
  * New upstream release.
    - Support for Transmission 1.8 and newer (Closes: #575897).
    - Do not raise an exception if the same torrent is being added twice,
      warn user instead (Closes: #569178).
  * Bump Standards-Version to 3.8.4, no changes required.

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Tue, 06 Apr 2010 22:44:05 +0200

transmissionrpc (0.3-2) unstable; urgency=low

  [ Luca Falavigna ]
  * Switch python-simplejson and python (>= 2.6) (build-)dependencies to
    avoid FTBFS in the buildds (Closes: #552714).

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Sun, 15 Nov 2009 20:09:03 +0100

transmissionrpc (0.3-1) unstable; urgency=low

  * Initial release. Closes: #531949 (ITP).

 -- Timur Birsh <taem@linukz.org>  Thu, 03 Sep 2009 15:26:41 +0600
